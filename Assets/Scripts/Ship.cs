﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

    /// <summary>
    ///     Ship's speed
    /// </summary>
    private float speed;

    /// <summary>
    /// Rotation speed
    /// </summary>
    public float rotationSpeed = 200.0f;

    /// <summary>
    ///     The maximum reachable speed
    /// </summary>
    public float maximumSpeed;

    private void UpdateFunction() {
        //transform.localPosition += new Vector3(0, 0, speed * Time.deltaTime);
        transform.position += transform.rotation * Vector3.up * speed * 0.1f;
        transform.Rotate(new Vector3(-Input.GetAxis("PitchKeyboard"), -Input.GetAxis("RollKeyboard"), -Input.GetAxis("YawKeyboard")));
    }

	// Use this for initialization
	void Start () {
        speed = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.UpArrow))
        {
            if(speed < maximumSpeed)
                speed++;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if(speed > 0) // Does not allow to go backward
                speed--;
        }
        Debug.Log("Roll " + Input.GetAxis("RollKeyboard"));
        Debug.Log("Pitch " + Input.GetAxis("PitchKeyboard"));
        Debug.Log("Yaw" + Input.GetAxis("YawKeyboard"));


        // Deccelerate when key not pressed
        if (speed > 0 && !Input.GetKey(KeyCode.UpArrow))
            speed--;
        UpdateFunction();
	}
}
