﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SelectionStep { category, weapon, infos }

public class Scroller : MonoBehaviour {

    private SelectionStep currentStep;
    private string selectedCategory;

    public Button backButton;
    public Gun gun;
    public GameObject listElement;

    /// <summary>
    /// Sets the category and increases the current step
    /// </summary>
    /// <param name="category">Selected category</param>
    private void SetCategory(string category) {
        selectedCategory = category;
        currentStep++;
        FlushList();
        ShowElements();
    }

    /// <summary>
    /// Shows the elements in the UI depending on the current step
    /// </summary>
    public void ShowElements() {
        switch(currentStep)
        {
            case SelectionStep.category:
                for (int i = 0; i < gun.numberOfCategories; i++)
                {
                    GameObject instantiatedElement = Instantiate(listElement);
                    instantiatedElement.transform.SetParent(GameObject.Find("Content").transform, false);

                    string categoryName = gun.categories[i];

                    //  Set text button
                    instantiatedElement.GetComponentInChildren<Text>().text = categoryName;

                    //  Set action
                    instantiatedElement.GetComponent<Button>().onClick.AddListener(delegate{ SetCategory(categoryName); });
                }
                break;

            case SelectionStep.weapon:
                List<Weapon> weapons = gun.GetWeapons();
                foreach (Weapon weapon in weapons)
                {
                    // Show the weapons for the selected category
                    if (gun.categories[weapon.GetCategoryId()] == selectedCategory)
                    {
                        Debug.Log(listElement);
                        GameObject instantiatedElement = Instantiate(listElement);
                        instantiatedElement.transform.SetParent(GameObject.Find("Content").transform, false);

                        //  Set text button
                        instantiatedElement.GetComponentInChildren<Text>().text = weapon.GetName();

                        //  Set action
                        instantiatedElement.GetComponent<Button>().onClick.AddListener(delegate { gun.SetSelectedWeapon(weapon); });
                    }
                }
                break;
            default:
                break;
        }
    }

    // Flushes all the elements contained in the scroller
    private void FlushList() {
        foreach (Transform child in GameObject.Find("Content").transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void BackButtonPressed() {

        // Empty the list and go to the previous step
        if(currentStep != SelectionStep.category)
        {
            FlushList();
            currentStep--;
            ShowElements();
        }

    }

	// Use this for initialization
	void Start () {
        FlushList();
        currentStep = SelectionStep.category;
        backButton.onClick.AddListener(BackButtonPressed);
    }
	
	// Update is called once per frame
	void Update () {
    }
}
