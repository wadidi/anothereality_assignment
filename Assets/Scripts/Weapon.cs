﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon {

    // private ---------------------------------------------
    private string name;
    private int rateOfFire;
    private int damage;
    private int cost;
    private string color;
    private int categoryId;

    public Weapon(string name, int rateOfFire, int damage, int cost, string color, int categoryId) {
        this.name = name;
        this.rateOfFire = rateOfFire;
        this.damage = damage;
        this.cost = cost;
        this.color = color;
        this.categoryId = categoryId;
    }

    // Getters & Setters -------------------------------------
    public string GetName() {
        return name;
    }

    public int GetCategoryId() {
        return categoryId;
    }
}
