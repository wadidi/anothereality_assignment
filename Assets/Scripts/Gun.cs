﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;

public class Gun : MonoBehaviour {

    // private ----------------------------------------------

    /// <summary>
    /// Defines how fast the energy increases : quantity/second
    /// </summary>
    private int rateOfRefill;

    /// <summary>
    /// List of all weapons as described in the XML file
    /// </summary>
    private List<Weapon> weapons;

    /// <summary>
    /// List of available weapons
    /// </summary>
    private List<Weapon> availableWeapons;

    /// <summary>
    /// Selected weapon
    /// </summary>
    private Weapon selectedWeapon;

    // public -------------------------------------------------

    /// <summary>
    /// Weapons file
    /// </summary>
    XmlDocument weaponsDoc;
    XmlNodeList nodesList;
    XmlNode root;
    [Tooltip("The XML describing the weapons")]
    public TextAsset weaponsFile;

    [NonSerialized]
    public int numberOfCategories = 0;
    [NonSerialized]
    public List<String> categories = new List<String>();

    // Getters & setters --------------------------------------

    public List<Weapon> GetWeapons() {
        return weapons;
    }

    public void SetSelectedWeapon(Weapon weapon) {
        selectedWeapon = weapon;
    }

    // Use this for initialization
    void Start () {
        // Fill the available weapons list

        //  Load the XML document
        weaponsDoc = new XmlDocument();
        weaponsDoc.LoadXml(weaponsFile.text);
        nodesList = weaponsDoc.SelectNodes("Weapons");
        root = nodesList[0];

        availableWeapons = new List<Weapon>();
        weapons = new List<Weapon>();

        // For each category, 
        foreach(XmlNode xnode in root.ChildNodes)
        {
            int categoryId = Convert.ToInt32(xnode.Attributes["id"].Value);
            numberOfCategories++;
            categories.Add(xnode.Name);

            //  For each weapon
            foreach(XmlNode weapon in xnode.ChildNodes)
            {
                string name = "";
                int rateOfFire = 0;
                int damage = 0;
                int cost = 0;
                string color = "";
                //  For each weapon's attribute
                foreach(XmlNode attribute in weapon)
                {
                    switch (attribute.Name)
                    {
                        case "Name":
                            name = attribute.InnerText;
                            break;
                        case "RateOfFire":
                            rateOfFire = Convert.ToInt32(attribute.InnerText);
                            break;
                        case "Damage":
                            damage = Convert.ToInt32(attribute.InnerText);
                            break;
                        case "Cost":
                            cost = Convert.ToInt32(attribute.InnerText);
                            break;
                        case "ColorCode":
                            color = attribute.InnerText;
                            break;
                        default:
                            break;
                    }
                }
                weapons.Add(new Weapon(name, rateOfFire, damage, cost, color, categoryId));
            }
        }
        GameObject.Find("Scroll View").GetComponent<Scroller>().ShowElements();
    }
	
	// Update is called once per frame
	void Update () {
	}
}
