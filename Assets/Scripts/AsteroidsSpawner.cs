﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsSpawner : MonoBehaviour {

    public GameObject asteroid;
    public GameObject shipCamera;
    public float spawnRadius = 100;

    /// <summary>
    /// Counts the number of asteroids currently in the scene
    /// </summary>
    private int totalNumberOfAsteroids;

    /// <summary>
    /// The number of asteroids that there should always be in the scene
    /// </summary>
    public int maximumNumberOfAsteroids;

    /// <summary>
    /// Makes an asteroid spawn not far from the camera
    /// </summary>
    public void SpawnRandom() {
        Vector3 cameraPosition = shipCamera.transform.position;
        Vector3 randomPosition = cameraPosition + Random.insideUnitSphere * spawnRadius;
        Instantiate(asteroid, randomPosition, Quaternion.identity);
        totalNumberOfAsteroids++;
    }

    /// <summary>
    ///     Counts the number of asteroids around the camera
    /// </summary>
    private int CountNumberOfSurroundingAsteroids() {
        return 0;
    }

    // Use this for initialization
    void Start () {
        totalNumberOfAsteroids = 0;

        //  Populate
        while (totalNumberOfAsteroids != maximumNumberOfAsteroids)
        {
            SpawnRandom();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (totalNumberOfAsteroids == 0)
            SpawnRandom();
	}
}
