# Brief of the project:

The project is kind of a space-sim fighter, like Elite Dangerous, or the X serie games (X2, X3...).

You, as a player, are sitting in a cockpit, and can pilot the ship through 3 axis (pitch, yaw, roll), and you can accelerate and decelerate at a maximum speed.

In this, you’ll have a weapom system commanded throuh a world-space UI residing in your cockpit, interactable through looking at it + confirming throuh mouse click.

The weapons are presented in different categories (lasers, missiles, and more categories unloced). Selecting a category will display the different type of lasers / missiles / etc, and selecting one of the weapons will open a display where there will be some data on the weapon (rate of fire, damage...) and a button to equip it on the ship.

There are some asteroids as static targets. They have some health and they respawn after x seconds when they’re destroyed.

Here’s the most important part: your ship has an “auto-targeting” system which locks a target within 30° from your crosshair. You can shoot projectiles from the guns, and they should be handled with physics, meaning they will also carry the initial velocity of the ship travelling.

Develop the auto-targeting system that can compensate his aim with the ship velocity / direction and always hits the target.

# Guideline:

You can work with cubes, spheres and capsules. Do not really bother with graphics, give just a rough idea of the concept (the ship, cockpit, weapons, projectiles, asteroids...).

The movement of the ship is not important, it is needed just for testing. You’re free here to use plugins or very simple code that does just the bare minimum work.

The main focus is creating a modular menu with dynamic display (if you have 3 weapons or 6 weapons it would be able to handle it without changing the code). Create 3 weapon variations for each category (could be laser with different color, size, firing speed...), make them recognizable when they get equipped on the ship. You can only have one weapon equipped at the time.

The UI of course should be usable, meaning having “back button”, an open / close button and with onHover effect.

Main challenge is the auto targeting system. Just think about that you have something shooting which carries also the momentum of your ship, and you have to always hit a target that is locked. Ideally you should visually show where the gun is actually going to shoot, but that’s a plus.

You can use assets / plugins for graphics, audio, textures and ship movement. UI system (besides what Unity already provides) and auto targeting should be custom made by yourself. Do your best